import gi
from cryptography.fernet import Fernet
import sqlite3
import os
import sys
import hashlib

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class RegistrationWindow(Gtk.Window):
    def __init__(self):
  
        # Call the constructor of the super class.
        # Set the value of the property title to Geeks for Geeks.
        Gtk.Window.__init__(self, title ="Register Password")

        # This will set the background image as the logo but the picture size needs to be changed
        background = Gtk.Image()
        background.set_from_file(os.path.join(sys.path[0], "Icons","image_cipher.png"))
        self.add(background)

        
        
        self.set_border_width(13)
        

        grid = Gtk.Grid()
        grid.set_row_spacing(5)
        self.add(grid)
        
        """
        Need to make two password boxes and if they don't match then they can't submit password
        """
        
        # Create Password box 1
        self.password_box1 = Gtk.Entry()
        self.password_box1.set_visibility(False)

        # Create Password box 2
        self.password_box2 = Gtk.Entry()
        self.password_box2.set_visibility(False)

        # Labels for password boxes
        self.label1 = Gtk.Label()
        self.label2 = Gtk.Label()

        self.label1.set_text("Insert Password")
        self.label2.set_text("Repeat Password")
        
        
        # Register password button
        register_pwd_button = Gtk.Button()
        register_pwd_button_img = Gtk.Image()
        register_pwd_button_img.set_from_file(os.path.join(sys.path[0], 'add.png'))
        register_pwd_button.add(register_pwd_button_img)
        register_pwd_button.connect("clicked", self.comp_pwd)
        register_pwd_button.set_size_request(20,10)


        # Attaching button to grid
        grid.attach(self.password_box1, 1, 1, 1, 1)
        grid.attach(self.label1, 1, 2, 1, 1)
        grid.attach(self.password_box2, 1, 3, 1, 1)
        grid.attach(self.label2, 1, 4, 1, 1)
        grid.attach(register_pwd_button, 1, 5, 1, 1)

        
        
    def comp_pwd(self, widget):
        pwd_1 = self.password_box1.get_text()
        pwd_2 = self.password_box2.get_text()
        if pwd_1 != pwd_2:
            print("Passwords aren't the same")
            dialog = Gtk.MessageDialog(
                transient_for=self,
                flags=0,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.CANCEL,
                )
            dialog.format_secondary_text(
                 "These passwords don't match, please try again"
             )
            dialog.run()
            
            self.password_box1.delete_text(0,100)
            self.password_box2.delete_text(0,100)
            
        elif pwd_1 == pwd_2:
            """
            pwd = self.password_box1.get_text()
            pwd = pwd.encode('utf-8')
            hashed_pwd = hashlib.sha3_512(pwd).hexdigest()
            sql = f"insert into {tb}(PWD) values (?)"
            cursor.execute(sql, (hashed_pwd, ))
            conn.commit()
            self.password_box1.delet_text(0, 100)
            key = Fernet.generate_key()
            with open('.key.key', 'wb') as file:
                file.write(key)   # Put dot before name so key is hidden
            Return the user to the original screen and give a pop up window
            saying that they have registered and now log in with their password
            """
            print("Call registration function to submit password to db")
            self.password_box1.delete_text(0, 100)
            self.password_box2.delete_text(0, 100)

            
win = RegistrationWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
