from cryptography.fernet import Fernet
import argparse

# R-gs0HHX5Z1O7Z-ZGYEuEs5EW5iJqoSOte99iI2uI7M=

def write_key():
    """
    Makes a key and saves it to a file
    """
    key = Fernet.generate_key()
    with open('key.key', 'wb') as key_file:
        key_file.write(key)


def load_key():
    """
    Loads the key generated in the write_key function
    """
    return open('key.key', "rb").read()

# we only need to write one key because if we write more than one it won't work
#write_key()

key = load_key()

f = Fernet(key)
"""
message = "Stuff write hur".encode()

encrypted = f.encrypt(message)
decrypted_encrypted = f.decrypt(encrypted)
"""

def encrypt(filename, key):
    """
    Takes a filename and a key and encrypts the file
    """
    f = Fernet(key)
    with open(filename, "rb") as file:
        file_data = file.read()

    encrypted_data = f.encrypt(file_data)

    with open(filename, "wb") as file:
        file.write(encrypted_data)

def decrypt(filename, key):
    """
    Given the filename and key this decrypts files
    """
    f = Fernet(key)
    
    with open(filename, "rb") as file:
        encrypted_data = file.read()

    decrypted_data = f.decrypt(encrypted_data)

    with open(filename, "wb") as file:
        file.write(decrypted_data)
    

        
parser = argparse.ArgumentParser()
parser.add_argument(
    "-p", action="store", dest="Photo",choices=['Encryption','Decryption'],help="File you want to encrypt/decrypt", type=str
)
parser.add_argument(
    "-f", action="store", dest="Filename", help="Choose the file you want to encrypt", type=str
)

args = parser.parse_args()
output = args.Photo
enc_file = args.Filename

if output == 'Encryption':
    encrypt(enc_file, key)

elif output == 'Decryption':
    decrypt(enc_file, key)

