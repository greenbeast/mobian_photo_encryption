import gi
from cryptography.fernet import Fernet
import sqlite3
import os
import sys
import hashlib
from shutil import move


gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

# Connecting to the database
conn = sqlite3.connect(".information2.db")
cursor = conn.cursor()
tb = "Password"


class photo_encryption(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Image Cypher")# WIP name(I think it is cool)
        """
        Basic app to encrypt photos for the Mobian OS 
        Though it would work on any linux OS, probably.
        """
        # background = Gtk.Image()
        # background.set_from_file(os.path.join(sys.path[0], "Icons","image_cipher.png"))
        # self.add(background)
        self.set_border_width(13)

        # Grid documentation https://athenajc.gitbooks.io/python-gtk-3-api/content/gtk-group/gtkgrid.html
        grid = Gtk.Grid()
        grid.set_row_spacing(5)
        self.add(grid)

        # Icon for window
        self.set_icon_from_file(os.path.join(sys.path[0], 'Icons','image_cipher.png'))

        # Password box
        self.password_box = Gtk.Entry()
        self.password_box.set_visibility(False)
        
        # Password submit button
        submit_button = Gtk.Button()
        submit_button_img = Gtk.Image()
        submit_button_img.set_from_file(os.path.join(sys.path[0], 'login.png'))
        submit_button.add(submit_button_img)
        submit_button.connect("clicked", self.hello)
        submit_button.set_size_request(20, 10)
        

        # Register Button
        register_button = Gtk.Button()#label="Submit")
        register_button_img = Gtk.Image()
        register_button_img.set_from_file(os.path.join(sys.path[0], 'add.png'))
        register_button.add(register_button_img)
        register_button.connect("clicked", self.create_password)
        register_button.set_size_request(20, 10)
        
        # Label
        self.label = Gtk.Label("Please Enter Password")
        self.connect('destroy', Gtk.main_quit)

        # Where everything is attached on the grid 
        grid.attach(submit_button, 1,3,1,1)
        grid.attach(self.password_box, 1,1,1,1)
        grid.attach(self.label, 1, 2, 1, 1)
        grid.attach(register_button, 1, 4, 1, 1)


        # loads key
        key = self.load_key()
        f = Fernet(key)
        
    # This will be the add button to add photos to the encrypted gallery
    # Documentation https://python-gtk-3-tutorial.readthedocs.io/en/latest/dialogs.html#filechooserdialog
    def hello(self, widget):
        output = self.password_box.get_text()
        print(output)
        # The link below is helpful for the specific file filtering
        # https://docs.huihoo.com/pygtk/2.0-tutorial/sec-FileChoosers.html
        dlg = Gtk.FileChooserDialog("Open..", action=Gtk.FileChooserAction.OPEN,buttons=(
                                                 Gtk.STOCK_CANCEL,
                                                 Gtk.ResponseType.CANCEL,
                                                 Gtk.STOCK_OPEN,
                                                 Gtk.ResponseType.OK))
        filters = Gtk.FileFilter()
        filters.set_name("Images")
        filters.add_mime_type("image/png")
        filters.add_mime_type("image/jpeg")
        filters.add_mime_type("image/jpg")
        filters.add_pattern(".jpeg")
        filters.add_pattern(".png")
        filters.add_pattern(".jpg")
        dlg.add_filter(filters)
        response = dlg.run()
        file_name = dlg.get_filename()
        # print(file_name)
        path, img_name = os.path.split(file_name)
        renamed_file = f"{path}/.{img_name}"
        move(file_name, renamed_file)
        print(renamed_file)
        dlg.destroy()
        # from import_photos import ImportWindow
        # var1 = ImportWindow
        # var1.show()
        
        
    # Might get rid of this function
    def create_db(self, widget):
        # Might need to have these outside so I can call them?
        try:
            conn = sqlite3.connect('passwords.db')
            c = conn.cursor()
            tb = "Passwords"
            if os.path.isfile('passwords.db'):
                pass
            else:
                create_table = f"CREATE TABLE IF NOT EXISTS {tb} (Password TEXT)"
                c.execute(create_table)
                insert_pwd = f"Insert into {tb} ('{submit_button.get_text()}')"
                c.execute(insert_pwd)
        except:
            pass

    
    # Not there yet with this but this will create a password when
    # you hit the register a password. Ideally though you will be taken
    # to a different screen and once you have one that option is now gone
    # rename to def register()
    def create_password(self, widget):
        from register import RegisterWindow
        # https://lazka.github.io/pgi-docs/Gtk-3.0/classes/LinkButton.html#Gtk.LinkButton
        # Use the above link to make it so that this isn't so much a button but rather
        # have it look like it is a link to registration which is probably more standard?
        # pwd = self.password_box.get_text()
        # pwd = pwd.encode('UTF-8')
        # # Using SHA3 512 hashing here
        # hashed_pwd = hashlib.sha3_512(pwd).hexdigest()
        # sql = f"""insert into {tb}(PWD) values (?)"""
        # cursor.execute(sql, (hashed_pwd,))
        # conn.commit()
        # self.password_box.delete_text(0, 100)
        # Need to call the create key thing

    """
    Maybe after signing in what I need to do is kill this 
    GTK script so that the other script (with the images and whatnot)
    is the only one.
    """
    def login(self, widget):
        pwd = self.password_box.get_text()
        pwd = pwd.encode('utf-8')
        hashed_pwd = hashlib.sha3_512(pwd).hexdigest()
        sql = f"select * from {tb} where PWD=(?)"
        output = cursor.execute(sql, (hashed_pwd, ))
        val = output.fetchone()
        if val != None:
            print("This would be a successfull login")
        else:
            # Put a dialog box to say wrong password
            print("Unfortunately this did not work...")
            dialog = Gtk.MessageDialog(
                transient_for=self,
                flags=0,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.CANCEL,
                #text="You have entered an incorrect password, please try again!",
                )
            dialog.format_secondary_text(
                 "You have entered an incorrect password, please try again!"
             )
            dialog.run()
            print("ERROR dialog closed")
            self.password_box.delete_text(0, 100)
            dialog.destroy()
        
        
    def write_key(self, widget):
        """
        Need to figure out how to only get this to run one time
        https://stackoverflow.com/questions/29764090/how-to-call-a-function-only-once-in-python
        Probably best to only run this when someone clicks the register button.
        """
        pass

    def load_key(self):
        """
        Loads the key generated in the write_key function
        """
        return open('key.key', "rb").read()
    
    def encrypt(self, widget, filename, key):
        print("Working")

    def decrypt(self, widget, filename, key):
        pass

app = photo_encryption()
app.show_all()
Gtk.main()
