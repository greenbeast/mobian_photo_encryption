import gi
from cryptography.fernet import Fernet
import sqlite3
import os
import sys
from shutil import move
import subproces as sp

"""
Here is a reddit thread on how to solve
the issue of have these open up multiple windows
rather than replacing the current window with 
what I want. 

https://www.reddit.com/r/AskProgramming/comments/nbr8f7/how_to_change_program_screen_with_python_gtk/
"""

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

"""
So what the plan is is to take the file_name
from what we get in the dialog box and rename it
to the same thing but with a . in front so now it is 
a hidden file and make a directory for them and then 
encrypt them

Currently I have it so the files are being renamed 
and saved in the same directory but now they are hidden
"""
# Define our own newWindow class.
class ImportWindow(Gtk.Window):
    def __init__(self):
        """
        This class will be what actually imports and encrypts
        the photos.
        """
        Gtk.Window.__init__(self, title ="Image Cypher")

        self.set_border_width(13)

        grid = Gtk.Grid()
        grid.set_row_spacing(5)
        self.add(grid)

        # Import button
        import_button = Gtk.Button()
        import_button_img = Gtk.Image()
        import_button_img.set_from_file(os.path.join(sys.path[0], 'Icons','add.png'))
        import_button.add(import_button_img)
        import_button.connect("clicked", self.photos)
        import_button.set_size_request(20,10)

        # Create gallery button
        # Icon for here <div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
        gallery_button = Gtk.Button()
        gallery_button_img = Gtk.Image()
        gallery_button_img.set_from_file(os.path.join(sys.path[0], 'Icons','gallery.png'))
        gallery_button.add(gallery_button_img)
        gallery_button.connect("clicked", self.create_gallery)
        gallery_button.set_size_request(20,10)

        # Label Telling what to do
        self.label = Gtk.Label("Please either import photos or create gallery")
        self.connect("destroy", Gtk.main_quit)
        
        # Attaching stuff to Grid
        grid.attach(import_button, 1, 2, 1, 1)
        grid.attach(gallery_button, 1, 4, 1, 1)
        grid.attach(self.label, 1, 1, 1, 1)

        key = self.load_key()
        f = Fernet(key)
        
    def load_key(self):
        """
        Loads the key generated in the write_key function 
        """
        return open('key.key', "rb").read()
        
        
    def photos(self, widget):
        """
        This will open the dialog box to import whatever photos 
        the user wants.
        """
        dlg = Gtk.FileChooserDialog("Open", action=Gtk.FileChooserAction.OPEN)
        dlg.add_buttons(Gtk.STOCK_CANCEL,
                        Gtk.ResponseType.CANCEL,
                        Gtk.STOCK_OPEN,
                        Gtk.ResponseType.OK)
        dlg.set_select_multiple(True)
        filters = Gtk.FileFilter()
        filters.set_name("Images")
        filters.add_mime_type("image/png")
        filters.add_mime_type("image/jpeg")
        filters.add_mime_type("image/jpg")
        filters.add_pattern(".jpeg")
        filters.add_pattern(".png")
        filters.add_pattern(".jpg")
        dlg.add_filter(filters)
        response = dlg.run()
        try:
            """
            Need to change this so that if a build already exists 
            it will instead just move files into the photos directory 
            made by the library. Or that can be done in the encryption library
            so that these get encryted then moved over to the new place because you don't want
            to be encrypting shit multiple times
            """
            file_name = dlg.get_filenames()
            for filee in file_name:
                path, img_name = os.path.split(filee)
                renamed_file = f"{path}/.__{img_name}"
                move(file_name, renamed_file)
                #print(renamed_file)
        except:
            print("You probably didn't pick a file")
        dlg.destroy()
        # encrypt()
        print("Importing photos in this biznatch")

    def encrypt():
        f = Fernet(key)
        # psuedo code here
        directory = sys.path[0]
        for filename in dir(directory):
            if filename.startwith(".__"):
                with open(filename, "rb") as file:
                    file_data = file.read()

                encrypted_data = f.encrypt(file_data)

                with open(filename, "wb") as file:
                    file.write(encrypted_data)
        

    def create_gallery(self, widget):
        print("Working")
        # Semi working script below
        """
        path = os.path.join(sys.path[0], 'public', 'index.html')
        if os.path.isfile(path):
            sp.call('gallery-build',shell=True)
            sp.call(f'xdg-open {path}', shell=True)
        else:
            sp.call('gallery-init', shell=True)
            sp.call(gallery-build, shell=True)
            sp.call(f'xdg-open {path}')
        
        To add photos to the album you can add it to the path
        ~/public/images/photos
        and then rebuild the gallery
        """


        
win = ImportWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
